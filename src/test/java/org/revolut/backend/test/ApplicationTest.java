package org.revolut.backend.test;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

/***
 * Using Hierarchical Context Runner to allow JUNITs to run in BDD format i.e. Given, When and Then
 */
@RunWith(HierarchicalContextRunner.class)
public class ApplicationTest {

    public class GivenApplication {

        public class WhenApplicationStarts {

            @Test
            public void thenApplicationShouldRun() {
                Application.main(new String[]{});
            }
        }
    }
}
