package org.revolut.backend.test.util;

import io.javalin.Context;
import io.javalin.core.HandlerType;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.revolut.backend.test.service.AccountService;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

import static org.revolut.backend.test.constant.TestConstants.URI_PATH_CREATE;

public class TestHelper {

    @Mock
    AccountService accountService;

    public ServletInputStream servletInputStream(byte[] myBytes) {

        ServletInputStream servletInputStream = new ServletInputStream() {
            private int lastIndexRetrieved = -1;
            private ReadListener readListener = null;

            @Override
            public boolean isFinished() {
                return (lastIndexRetrieved == myBytes.length - 1);
            }

            @Override
            public boolean isReady() {
                // This implementation will never block
                // We also never need to call the readListener from this method, as this method will never return false
                return isFinished();
            }

            @Override
            public void setReadListener(ReadListener readListener) {
                this.readListener = readListener;
                if (!isFinished()) {
                    try {
                        readListener.onDataAvailable();
                    } catch (IOException e) {
                        readListener.onError(e);
                    }
                } else {
                    try {
                        readListener.onAllDataRead();
                    } catch (IOException e) {
                        readListener.onError(e);
                    }
                }
            }

            @Override
            public int read() throws IOException {
                int i;
                if (!isFinished()) {
                    i = myBytes[lastIndexRetrieved + 1];
                    lastIndexRetrieved++;
                    if (isFinished() && (readListener != null)) {
                        try {
                            readListener.onAllDataRead();
                        } catch (IOException ex) {
                            readListener.onError(ex);
                            throw ex;
                        }
                    }
                    return i;
                } else {
                    return -1;
                }
            }
        };
        return servletInputStream;
    }

    public void createDummyAccount(int numberOfAccount) throws Exception {

        for (int i = 0; i < numberOfAccount; i++) {

            String TEST_DATA = "{\n" +
                    "  \"account\": \"ACCOUNT\",\n" +
                    "  \"amount\": \"AMOUNT\"\n" +
                    "}";

            TEST_DATA = TEST_DATA.replace("ACCOUNT", String.valueOf(generateRandomLongValue()));
            TEST_DATA = TEST_DATA.replace("AMOUNT", generateAmount().toString());

            HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
            HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);

            ArrayList testArrayList = new ArrayList();
            HashMap<String, String> testHashMap = new HashMap<>();
            Context ctx = new Context(httpServletResponse,
                    httpServletRequest, true, URI_PATH_CREATE,
                    testHashMap, testArrayList, HandlerType.POST);

            HttpServletRequestWrapper httpServletRequestWrapper = Mockito.mock(HttpServletRequestWrapper.class);
            httpServletRequestWrapper.setRequest(httpServletRequest);

            final byte[] myBytes = TEST_DATA.getBytes();
            ServletInputStream servletInputStream = servletInputStream(myBytes);

            Mockito.when(httpServletRequest.getInputStream()).thenReturn(servletInputStream);
            Mockito.when(ctx.request().getInputStream()).thenReturn(servletInputStream);
            httpServletRequest.getInputStream();
            ctx.request().getInputStream();
            accountService.createAccount.handle(ctx);
        }
    }

    public BigDecimal generateAmount() {
        BigDecimal amount = new BigDecimal(100.00);

        return amount.setScale(2, RoundingMode.CEILING);
    }

    public long generateRandomLongValue() {
        long minLong = 1000L;
        long maxLong = 100000L;

        return minLong + (long) (Math.random() * (maxLong - minLong));
    }
}
