package org.revolut.backend.test.util;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.revolut.backend.test.constant.TestConstants.*;

@RunWith(HierarchicalContextRunner.class)
public class PropertyFileReaderTest {

    @Mock
    PropertyFileReader propertyFileReader;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    public class GivenPropertyValueRequired {

        public class WhenPropertyValueReadRequested {

            @Test
            public void thenCorrectValueMustBeReturned() {
                Mockito.when(propertyFileReader.propReader(APPLICATION_PROPERTIES, TEST_PROPERTY)).thenCallRealMethod();
                Assert.assertEquals(propertyFileReader.propReader(APPLICATION_PROPERTIES, TEST_PROPERTY), TEST_VALUE);

            }
        }
    }
}
