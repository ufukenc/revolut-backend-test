package org.revolut.backend.test.constant;

import java.math.BigDecimal;

public class TestConstants {
    public static final Long HASH_MAP_KEY = 12345L;
    public static final BigDecimal HASH_MAP_VALUE = new BigDecimal(99.99);
    public static final BigDecimal AFTER_TRANSFER = new BigDecimal(105.50);
    public static final String URI_PATH_CREATE = "/account/create";
    public static final String URI_PATH_LIST = "/account/list";
    public static final String URI_PATH_TRANSFER = "/account/transfer";

    public static final String APPLICATION_PROPERTIES = "application.properties";
    public static final String TEST_PROPERTY = "test.test";
    public static final String TEST_VALUE = "test";

    public static final String TEST_DATA = "{\n" +
            "  \"account\": \"12345\",\n" +
            "  \"amount\": \"99.99\"\n" +
            "}";

    public static String TEST_TRANSFER_DATA = "{\n" +
            "\t\"fromAccount\": \"FROM_ACCOUNT\",\n" +
            "\t\"toAccount\": \"TO_ACCOUNT\",\n" +
            "\t\"amount\": \"5.50\"\n" +
            "}";
}
