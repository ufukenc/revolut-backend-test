package org.revolut.backend.test.service;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import de.bechte.junit.runners.context.HierarchicalContextRunner;
import io.javalin.Context;
import io.javalin.core.HandlerType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.revolut.backend.test.util.TestHelper;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.revolut.backend.test.constant.TestConstants.*;
import static org.revolut.backend.test.service.AccountService.accountsModel;

@RunWith(HierarchicalContextRunner.class)
public class AccountServiceTest {

    @Mock
    AccountService accountService;

    @Mock
    private Appender mockAppender;

    @Captor
    private ArgumentCaptor<LoggingEvent> eventArgumentCaptor;

    TestHelper testHelper = new TestHelper();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(mockAppender);
    }

    public class GivenCreateAccount {

        public class WhenCreateAccountEndpointRequestedForDifferentAccounts {

            @Before
            public void setup() {
                accountsModel.conHashMap.clear();
            }

            @Test
            public void thenAccount12345ShouldBeCreated() throws Exception {
                HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
                HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);

                ArrayList testArrayList = new ArrayList();
                HashMap<String, String> testHashMap = new HashMap<>();
                Context ctx = new Context(httpServletResponse,
                        httpServletRequest, true, URI_PATH_CREATE,
                        testHashMap, testArrayList, HandlerType.POST);

                HttpServletRequestWrapper httpServletRequestWrapper = Mockito.mock(HttpServletRequestWrapper.class);
                httpServletRequestWrapper.setRequest(httpServletRequest);

                final byte[] myBytes = TEST_DATA.getBytes();
                ServletInputStream servletInputStream = testHelper.servletInputStream(myBytes);

                Mockito.when(httpServletRequest.getInputStream()).thenReturn(servletInputStream);
                Mockito.when(ctx.request().getInputStream()).thenReturn(servletInputStream);
                httpServletRequest.getInputStream();
                ctx.request().getInputStream();
                accountService.createAccount.handle(ctx);
                Assert.assertEquals(accountsModel.conHashMap.size(), 1);
                Assert.assertEquals(accountsModel.conHashMap.get(HASH_MAP_KEY).compareTo(HASH_MAP_VALUE), 1);
            }
        }
    }

    public class GivenMultipleAccountsCreated {

        @Before
        public void setup() throws Exception {
            accountsModel.conHashMap.clear();
            testHelper.createDummyAccount(2);
        }

        public class WhenListAccountEndPointIsRequested {

            @Test
            public void thenNumberOfAccountsMustExist() throws Exception {
                HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
                HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
                ArrayList testArrayList = new ArrayList();
                HashMap<String, String> testHashMap = new HashMap<>();

                Context ctx = new Context(httpServletResponse,
                        httpServletRequest, true, URI_PATH_LIST,
                        testHashMap, testArrayList, HandlerType.GET);

                accountService.listAccounts.handle(ctx);

                Mockito.verify(mockAppender, Mockito.atLeastOnce()).doAppend(eventArgumentCaptor.capture());
                String numberOfAccounts = eventArgumentCaptor.getAllValues().get(5).getMessage();
                Assert.assertTrue(numberOfAccounts.contains("2"));
            }
        }
    }

    public class GivenTransferRequested {

        @Before
        public void setup() throws Exception {
            accountsModel.conHashMap.clear();
            testHelper.createDummyAccount(2);
        }

        public class WhenValidTransferRequestSent {

            @Test
            public void thenMoneyShouldbeTransferedSuccessfully() throws Exception {
                List<Long> accounts = new ArrayList<>();
                HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
                HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);

                ArrayList testArrayList = new ArrayList();
                HashMap<String, String> testHashMap = new HashMap<>();
                Context ctx = new Context(httpServletResponse,
                        httpServletRequest, true, URI_PATH_TRANSFER,
                        testHashMap, testArrayList, HandlerType.POST);

                HttpServletRequestWrapper httpServletRequestWrapper = Mockito.mock(HttpServletRequestWrapper.class);
                httpServletRequestWrapper.setRequest(httpServletRequest);

                for (Map.Entry<Long, BigDecimal> entry : accountsModel.conHashMap.entrySet()) {
                    accounts.add(entry.getKey());
                }
                TEST_TRANSFER_DATA = TEST_TRANSFER_DATA
                        .replace("FROM_ACCOUNT", String.valueOf(accounts.get(0)));
                TEST_TRANSFER_DATA = TEST_TRANSFER_DATA.replace("TO_ACCOUNT", String.valueOf(accounts.get(1)));

                final byte[] myBytes = TEST_TRANSFER_DATA.getBytes();
                ServletInputStream servletInputStream = testHelper.servletInputStream(myBytes);

                Mockito.when(httpServletRequest.getInputStream()).thenReturn(servletInputStream);
                Mockito.when(ctx.request().getInputStream()).thenReturn(servletInputStream);
                httpServletRequest.getInputStream();
                ctx.request().getInputStream();
                accountService.transferMoney.handle(ctx);
                Assert.assertEquals(accountsModel.conHashMap.size(), 2);
                Assert.assertEquals(accountsModel.conHashMap.get(accounts.get(1)).compareTo(AFTER_TRANSFER), 0);
            }
        }
    }

}
