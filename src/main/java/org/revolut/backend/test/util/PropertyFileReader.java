package org.revolut.backend.test.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.revolut.backend.test.constant.LogConstant.UNEXPECTED_ERROR;

/***
 * Property file reader util
 */
public class PropertyFileReader {

    private static final Logger logger = LoggerFactory.getLogger(PropertyFileReader.class);


    /**
     * Function to read property files. It will intake property file and property name
     * it will then return the property value.
     *
     * @param propertyFile
     * @param property
     * @return
     */
    public String propReader(String propertyFile, String property) {
        String propertyValue = null;
        try (InputStream input = PropertyFileReader.class.getClassLoader()
                .getResourceAsStream(propertyFile)) {
            Properties properties = new Properties();

            properties.load(input);

            propertyValue = properties.getProperty(property);

        } catch (IOException ioe) {
            logger.error(UNEXPECTED_ERROR, ioe.getMessage());
        }
        return propertyValue;
    }
}
