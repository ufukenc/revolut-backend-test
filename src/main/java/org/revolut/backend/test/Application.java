package org.revolut.backend.test;

import io.javalin.Javalin;
import org.revolut.backend.test.service.AccountService;

public class Application {

    public static void main(String[] args) {
        Javalin app = Javalin.create()
                .port(8080)
                .start();

        app.post("/account/transferMoney", AccountService.transferMoney);
        app.post("/account/create", AccountService.createAccount);
        app.get("/account/list", AccountService.listAccounts);
    }
}
