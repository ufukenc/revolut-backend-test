package org.revolut.backend.test.constant;

/**
 * Constants class for all logs
 */
public class LogConstant {

    public static final String UNEXPECTED_ERROR = "UNEXPECTED_ERROR";

    public static final String MONEY_TRANSFER_INITIATED = "MONEY_TRANSFER_INITIATED";
    public static final String MONEY_TRANSFER_COMPLETED = "MONEY_TRANSFER_COMPLETED";
    public static final String MONEY_TRANSFER_FAILED = "MONEY_TRANSFER_FAILED";

    public static final String ACCOUNT_CREATION_INITIATED = "ACCOUNT_CREATION_INITIATED";
    public static final String ACCOUNT_CREATION_COMPLETED = "ACCOUNT_CREATION_COMPLETED";
    public static final String ACCOUNT_CREATION_FAILED = "ACCOUNT_CREATION_FAILED";

    public static final String LIST_ACCOUNT_DETAILS_INITIATED = "LIST_ACCOUNT_DETAILS_INITIATED";
    public static final String NUMBER_OF_ACCOUNTS = "NUMBER_OF_ACCOUNTS";

    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
}
