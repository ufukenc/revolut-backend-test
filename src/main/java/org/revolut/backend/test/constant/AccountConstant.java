package org.revolut.backend.test.constant;

public class AccountConstant {

    public static final String APPLICATION_PROPERTIES = "application.properties";
    public static final String ERROR_ACCOUNT_NOT_FOUND = "error.account.not.found";
    public static final String ERROR_NOT_ENOUGH_BALANCE = "error.not.enough.balance";
}
