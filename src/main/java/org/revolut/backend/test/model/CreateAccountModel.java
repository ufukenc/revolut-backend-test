package org.revolut.backend.test.model;

import lombok.Data;

import java.math.BigDecimal;

/***
 * Create Account Model
 * A model to be used when creating a new account
 */
@Data
public class CreateAccountModel {
    private Long account;
    private BigDecimal amount = new BigDecimal(0);
}
