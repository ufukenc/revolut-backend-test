package org.revolut.backend.test.model;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

/***
 * AccountsModel to store the accounts in memory
 */
public class AccountsModel {

    public ConcurrentHashMap<Long, BigDecimal> conHashMap = new ConcurrentHashMap<>();
}
