package org.revolut.backend.test.model;

import lombok.Data;

/***
 * Generic Exception model to set error message on
 * errors returned to end user
 */
@Data
public class ExceptionModel {

    private String errorMessage;
}
