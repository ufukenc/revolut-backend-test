package org.revolut.backend.test.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/***
 * Transfer model
 * This is the request model which will be used when sending POST request
 */
@Getter
@Setter
@ToString
public class TransferModel {

    private Long fromAccount;

    private Long toAccount;

    private BigDecimal amount = new BigDecimal(0);
}
