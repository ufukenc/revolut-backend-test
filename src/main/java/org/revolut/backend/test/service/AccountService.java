package org.revolut.backend.test.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Handler;
import org.eclipse.jetty.http.HttpStatus;
import org.revolut.backend.test.model.AccountsModel;
import org.revolut.backend.test.model.CreateAccountModel;
import org.revolut.backend.test.model.ExceptionModel;
import org.revolut.backend.test.model.TransferModel;
import org.revolut.backend.test.util.PropertyFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.revolut.backend.test.constant.AccountConstant.*;
import static org.revolut.backend.test.constant.LogConstant.*;

public class AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    static AccountsModel accountsModel = new AccountsModel();

    static PropertyFileReader propertyFileReader = new PropertyFileReader();

    static ConcurrentHashMap<Long, BigDecimal> concurrentHashMap = new ConcurrentHashMap<>();

    static ObjectMapper objectMapper = new ObjectMapper();

    /***
     * Function to create a new account in the hash map. If the requested contract does not
     * match the expected contract, it will return 400 bad request error
     */
    public static Handler createAccount = ctx -> {
        logger.info(ACCOUNT_CREATION_INITIATED);
        try {
            CreateAccountModel createAccountModel = objectMapper.readValue(ctx.body().trim(),
                    CreateAccountModel.class);

            concurrentHashMap.put(Long.parseLong(createAccountModel.getAccount().toString()),
                    createAccountModel.getAmount());

            accountsModel.conHashMap = concurrentHashMap;

            logger.info(ACCOUNT_CREATION_COMPLETED + ", " +
                    ACCOUNT_NUMBER + ": " +
                    createAccountModel.getAccount().toString());

        } catch (IOException ioe) {
            logger.error(ACCOUNT_CREATION_FAILED + ", " + UNEXPECTED_ERROR + ", " + ioe.getMessage());
            ExceptionModel exceptionModel = new ExceptionModel();
            exceptionModel.setErrorMessage(ioe.getMessage().trim());
            ctx.json(exceptionModel);
            ctx.status(HttpStatus.BAD_REQUEST_400);
        }
    };

    /**
     * Function to list all accounts stored in the hash map
     *
     * @return OK_200
     */
    public static Handler listAccounts = ctx -> {
        logger.info(LIST_ACCOUNT_DETAILS_INITIATED);
        try {
            List<CreateAccountModel> accountsModels = new ArrayList<>();
            for (Map.Entry<Long, BigDecimal> entry : accountsModel.conHashMap.entrySet()) {
                CreateAccountModel createAccountModel = new CreateAccountModel();
                createAccountModel.setAccount(entry.getKey());
                createAccountModel.setAmount(entry.getValue());
                accountsModels.add(createAccountModel);
            }
            logger.info(NUMBER_OF_ACCOUNTS + ", " + accountsModels.size());
            ctx.json(accountsModels);
            ctx.status(HttpStatus.OK_200);
        } catch (Exception ex) {
            logger.error(UNEXPECTED_ERROR + ", " + ex.getMessage());
            ExceptionModel exceptionModel = new ExceptionModel();
            exceptionModel.setErrorMessage(ex.getMessage().trim());
            ctx.json(exceptionModel);
            ctx.status(HttpStatus.BAD_REQUEST_400);
        }
    };

    /**
     * Function to action the transfer request.
     * This function will check the accounts, balance and then transfer the money accordingly
     * <p>
     * If anything goes wrong, it will return i.e. 404 not found or 400 bad request response
     * to consumer service
     */
    public static Handler transferMoney = ctx -> {
        logger.info(MONEY_TRANSFER_INITIATED);

        try {
            TransferModel transferModel = objectMapper.readValue(ctx.body(), TransferModel.class);

            // Checking if the from account and to account exists in the hash map
            if (accountsModel.conHashMap.containsKey(transferModel.getFromAccount()) &&
                    accountsModel.conHashMap.containsKey(transferModel.getToAccount())) {
                // if the accounts exits, we carry out a balance check to see if from account has enough balance
                // to make a transfer
                if (accountsModel.conHashMap.get(transferModel.getFromAccount()).compareTo(
                        transferModel.getAmount()) >= 0) {
                    accountsModel.conHashMap.put(transferModel.getFromAccount(),
                            accountsModel.conHashMap.get(transferModel.getFromAccount()).subtract(
                                    transferModel.getAmount()
                            ));
                    accountsModel.conHashMap.put(transferModel.getToAccount(),
                            accountsModel.conHashMap.get(transferModel.getToAccount()).add(
                                    transferModel.getAmount()
                            ));
                    logger.info(MONEY_TRANSFER_COMPLETED);
                } else {
                    logger.error(MONEY_TRANSFER_FAILED + ", " + ERROR_NOT_ENOUGH_BALANCE);
                    ExceptionModel exceptionModel = new ExceptionModel();
                    exceptionModel.setErrorMessage(propertyFileReader.propReader(APPLICATION_PROPERTIES,
                            ERROR_NOT_ENOUGH_BALANCE));
                    ctx.json(exceptionModel);
                    ctx.status(HttpStatus.BAD_REQUEST_400);
                }
            } else {
                logger.error(MONEY_TRANSFER_FAILED + ", " + ERROR_ACCOUNT_NOT_FOUND);
                ExceptionModel exceptionModel = new ExceptionModel();
                exceptionModel.setErrorMessage(propertyFileReader.propReader(APPLICATION_PROPERTIES,
                        ERROR_ACCOUNT_NOT_FOUND));
                ctx.json(exceptionModel);
                ctx.status(HttpStatus.NOT_FOUND_404);
            }
        } catch (Exception ex) {
            logger.error(UNEXPECTED_ERROR + ", " + ex.getMessage());
            ExceptionModel exceptionModel = new ExceptionModel();
            exceptionModel.setErrorMessage(ex.getMessage());
            ctx.json(exceptionModel);
            ctx.status(HttpStatus.BAD_REQUEST_400);
        }
    };
}
