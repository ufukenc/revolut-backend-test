# Revolut Backend Test

### Backend Test Requirements
Design and implement a RESTful API (including data org.revolut.home.task.model and the backing implementation) for money transfers between accounts.

#### Explicit requirements:
1. You can use Java, Scala or Kotlin.
2. Keep it simple and to the point (e.g. no need to implement any authentication).
3. Assume the API is invoked by multiple systems and services on behalf of end users.
4. You can use frameworks/libraries if you like (except Spring), but don't forget about requirement #2 – keep it simple and avoid heavy frameworks.
5. The datastore should run in-memory for the sake of this test.
6. The final result should be executable as a standalone program (should not require a pre-installed container/server).
7. Demonstrate with tests that the API works as expected.

#### Implicit requirements:
1. The code produced by you is expected to be of high quality.
2. There are no detailed requirements, use common sense.

### Build and Run
```
Build: mvn clean install
Run:   java -jar org.revolut.account-0.1-jar-with-dependencies.jar
```

### Build
```
mvn clean install
```

### Endpoints
1. Create Account
    ```
        http://localhost:8080/account/create
    ```
    POST Method
- Sample Payload
    ```
    {
        "account": "12345",
        "amount": "100.00"
    }
    ```
2. List Account
    ```
        http://localhost:8080/account/list
    ```
    GET Method
- Sample Payload
    ```
    [
        {
            "account": 1234,
            "amount": 100
        },
        {
            "account": 123,
            "amount": 100
        }
    ]
    ```
3. Transfer Money
    ```
        http://localhost:8080/account/transfer
    ```
    POST Method
- Sample Payload
    ```
    {
        "fromAccount": "FROM_ACCOUNT",
        "toAccount": "TO_ACCOUNT",
        "amount": "5.50"
    }
    ```

### Improvements
1. Improve unit testing. Currently don't have detailed unit tests due to lack of understanding on Javalin Framework.